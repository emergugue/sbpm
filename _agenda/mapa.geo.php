<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>Display latitude longitude on marker movement</title>

<script src="http://cdn.leafletjs.com/leaflet-0.7/leaflet.js"></script>

<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7/leaflet.css" />
<style>
  body { margin:0; padding:0; }
  #map {width: 100%;height: 280px;}
</style>
</head>
<body onload="javascript:window.parent.document.getElementById('<?php echo $_REQUEST[id]?>').value= '';">



<div id='map'></div>
<?php 
		if ($_REQUEST[lat] !='') {$lat=$_REQUEST[lat];}else {$lat= "-75.5570125579834";}
		if ($_REQUEST[lon] !='') {$lon=$_REQUEST[lon];}else {$lon= "6.2463742841860";}
		if ($_REQUEST[zoom] !='') {$zoom=$_REQUEST[zoom];}else {$zoom= "16";}

 ?>
<script>


	var map = 	L.map('map').setView([<?php echo $lon ?>, <?php echo $lat ?>], <?php echo $zoom ?>);
					L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

function onLocationFound(e) {
			var radius = e.accuracy / 2;
			<?php if ($_REQUEST[lat] =='') {
				echo "var marker = L.marker(e.latlng,{draggable: true}).addTo(map);";
				}else{
				echo "var marker = L.marker([$lon,$lat],$zoom,{draggable: true}).addTo(map);";	
				} ?>

				marker.on('dragend', ondragend);
						ondragend();
						function ondragend() {
						    var m = marker.getLatLng();
						    var z = map.getZoom();
						//   window.parent.document.getElementById('<?php echo $_REQUEST[id]?>').value= m.lng+' '+m.lat+' '+z;
						    window.parent.document.getElementById('<?php echo $_REQUEST[id]?>').value= m.lng+' '+m.lat+' '+z;
													}
									}
function onLocationError(e) {
				var marker = L.marker([<?php echo $lon ?>,<?php echo $lat ?>],{draggable: true}).addTo(map);
				marker.on('dragend', ondragend);
						ondragend();
				function ondragend() {
						    var m = marker.getLatLng();
						    var z = map.getZoom();

 //   window.parent.document.getElementById('<?php echo $_REQUEST[id]?>').value= m.lng+' '+m.lat+' '+z;
    window.parent.document.getElementById('<?php echo $_REQUEST[id]?>').value= m.lng+' '+m.lat+' '+z;
											}
									}

		map.on('locationfound', onLocationFound);
		map.on('locationerror', onLocationError);
		map.locate({setView: true, maxZoom: 18});
</script>

<input id="campo" value='hola'>
</body>
</html>