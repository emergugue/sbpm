<?php
/* Template Name: Buscador Específico Eventos */
$posts = array();
$categories = get_terms( array(
    'taxonomy' => 'event-categories',
    'hide_empty' => false,
));
$audiences = get_terms( array(
    'taxonomy' => 'event_audiences',
    'hide_empty' => false,
));

$options = array(
    'post_type' => 'event', // the search query
	'posts_per_page' => -1
);
$terms    = (!empty($_GET['terms'])) ? $_GET['terms']       : false;
$category = (!empty($_GET['category'])) ? $_GET['category'] : false;
$audience = (!empty($_GET['audience'])) ? $_GET['audience'] : false;
$start    = (!empty($_GET['start'])) ? $_GET['start']       : date('m-d-Y');
$end      = (!empty($_GET['end'])) ? $_GET['end']           : strtotime(date("Y-m-d", strtotime($date)) . " +12 month");

$options['tax_query'] = array();

if( $terms ){
    $options['s'] = strip_tags(trim($terms));
}
if( $category ){
    $options['tax_query'][] = 		array(
		'taxonomy' => 'event-categories',
		'terms'    => $category,
	);
}
if( $audience ){
    $options['tax_query'][] = 		array(
		'taxonomy' => 'event_audiences',
		'terms'    => $audience,
	);
}
if( $start ){
    if($end){
        $options['meta_key']     = '_event_start_date';
        $options['meta_value']   = array($start, $end);
        $options['meta_compare'] = 'BETWEEN';
    } else {
        $options['meta_key']     = '_event_start_date';
        $options['meta_value']   = $start;
        $options['meta_compare'] = '=';
    }
}
get_header(); ?>
<div class="wrap row">
    <h2>Prográmate en nuestras bibliotecas</h2>
    <form class="SearchCalendar">
        <div class="SearchCalendar-cols SearchCalendar-cols-3">
            <div class="SearchCalendar-group SearchCalendar-col-1">
                <label for="terms">Búsqueda por palabra clave</label>
                <input type="text" name="terms" placeholder="Ingrese una palabra clave" value="<?php echo $terms ?>">
            </div>
            <div class="SearchCalendar-group SearchCalendar-col-2">
                <label for="category">Selecciona el tipo de evento</label>
                <select name="category">
                    <option value>Categoría</option>
                    <?php foreach( $categories as $cat ): ?>
                        <option value="<?= $cat->term_id ?>" <?php echo ($cat->term_id == $category) ? 'selected' : '' ?>><?= $cat->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="SearchCalendar-group SearchCalendar-col-3">
                <label for="audience">Selecciona el tipo de público</label>
                <select name="audience">
                    <option value>Público</option>
                    <?php foreach( $audiences as $a ): ?>
                        <option value="<?= $a->term_id ?>" <?php echo ($a->term_id == $audience) ? 'selected' : '' ?>><?= $a->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="SearchCalendar-group">
            <p class="SearchCalendar-textLabel">Selecciona el rango de fechas</p>
            <div class="SearchCalendar-cols">
                <div class="SearchCalendar-col1">
                    <label for="start">De</label>
                    <input type="date" name="start" value="<?php echo $start ?>">
                </div>
                <div class="SearchCalendar-col2">
                    <label for="end">A</label>
                    <input type="date" name="end" value="<?php echo $end ?>">
                </div>
            </div>
        </div>
        <div class="SearchCalendar-group SearchCalendar-btn">
            <input type="submit">
        </div>
    </form>
<?php


$query = new WP_Query(
    $options
);
while ( $query->have_posts() ) {
    $query->next_post();
    $posts[] = array('post' => $query->post, 'url' => get_the_permalink($query->post) );
}
?>

    <div class="Results">
        <ul class="BlockList">
            <?php foreach ( $posts as $networkpost ): ?>
                <li class="BlockList-item">
                    <h3 class="BlockList-itemTitle"><a href="<?php echo $networkpost['url'] ?>"><?php echo $networkpost['post']->post_title ?></a></h3>
                    <p class="BlockList-itemDescription"><?php echo $networkpost['post']->post_content ?></p>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Lugar: </span><span class="BlockList-itemText--Small"><?php echo event_location($networkpost['post']->ID); ?></span>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Fecha: </span><span class="BlockList-itemText--XSmall"><?php echo event_start_date($networkpost['post']->ID); ?></span>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Dirección: </span><span class="BlockList-itemText--XSmall"><?php echo event_start_time($networkpost['post']->ID); ?></span>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div> <!--/wrap-->
<?php get_footer(); ?>
