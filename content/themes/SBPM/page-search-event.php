<?php
/* Template Name: Buscador General Eventos */

global $wp_query;

$terms      	= (!empty($_GET['terms']))    ? $_GET['terms']            : false;
$library    	= (!empty($_GET['library']))  ? $_GET['library']          : false;
$category   	= (!empty($_GET['category'])) ? $_GET['category']         : false;
$audience   	= (!empty($_GET['audience'])) ? $_GET['audience']         : false;
$start      	= (!empty($_GET['start']))    ? $_GET['start']            : date('Y-m-d');
$end        	= (!empty($_GET['end']))      ? $_GET['end']              : date('Y-m-d', strtotime('+1 years'));
$paged      	= (get_query_var('paged'))    ? get_query_var('paged')    : 1;
$posts      	= array();
$sites      	= get_sites();
$pagination 	= "";
$query_string 	= $_SERVER['QUERY_STRING'];
$url_path		= get_page_link();

$categories = get_terms( array(
    'taxonomy' => 'event-categories',
    'hide_empty' => false,
));

$audiences = get_terms( array(
    'taxonomy' => 'event_audiences',
    'hide_empty' => false,
));

$options = array(
    'post_type' => 'event',
    'posts_per_page' => 15,
    'paged' => $paged,
    'order_by' => '_event_start_date',
    'order' => 'ASC',
);

$options['tax_query'] = array();

if( $terms ){
    $options['s'] = strip_tags(trim($terms));
}

if( $category ){
    $options['tax_query'][] = 		array(
		'taxonomy' => 'event-categories',
		'terms'    => (int) $category,
	);
}

if( $audience ){
    $options['tax_query'][] = 		array(
		'taxonomy' => 'event_audiences',
		'terms'    => (int) $audience,
	);
}

if( $start ){
    if($end){
        $options['meta_key']     = '_event_start_date';
        $options['meta_value']   = array($start, $end);
        $options['meta_compare'] = 'BETWEEN';
    } else {
        $options['meta_key']     = '_event_start_date';
        $options['meta_value']   = array($start, $end);
        $options['meta_compare'] = 'BETWEEN';
    }
}

get_header(); ?>
<div class="wrap row">
    <h2><?php the_title(); ?></h2>
    <?php get_template_part('loop', 'page'); ?>
    <form class="SearchCalendar">
        <div class="SearchCalendar-group">
            <label for="terms">Búsqueda por palabra clave</label>
            <input type="text" name="terms" placeholder="Ingrese una palabra clave" value="<?php echo $terms ?>">
        </div>
        <div class="SearchCalendar-cols SearchCalendar-cols-3">
            <div class="SearchCalendar-group SearchCalendar-col-1">
                <label for="library">Selecciona la biblioteca</label>
                <select name="library">
                    <option value>Seleccione Biblioteca</option>
                    <?php foreach( $sites as $site ): ?>
                        <?php $name = get_blog_details($site->blog_id)->blogname; ?>
                        <option value="<?= $site->blog_id ?>" <?php echo ($site->blog_id == $library) ? 'selected' : '' ?>><?= $name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="SearchCalendar-group SearchCalendar-col-2">
                <label for="category">Selecciona el tipo de evento</label>
                <select name="category">
                    <option value>Categoría</option>
                    <?php foreach( $categories as $cat ): ?>
                        <option value="<?= $cat->term_id ?>" <?php echo ($cat->term_id == $category) ? 'selected' : '' ?>><?= $cat->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="SearchCalendar-group SearchCalendar-col-3">
                <label for="audience">Selecciona el tipo de público</label>
                <select name="audience">
                    <option value>Público</option>
                    <?php foreach( $audiences as $a ): ?>
                        <option value="<?= $a->term_id ?>" <?php echo ($a->term_id == $audience) ? 'selected' : '' ?>><?= $a->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div> <!--/SearchCalendar-cols-->
        <div class="SearchCalendar-group">
            <p class="SearchCalendar-textLabel">Selecciona el rango de fechas</p>
            <div class="SearchCalendar-cols">
                <div class="SearchCalendar-col1">
                    <label for="start">De</label>
                    <input type="date" name="start" value="<?php echo $start ?>">
                </div>
                <div class="SearchCalendar-col2">
                    <label for="end">A</label>
                    <input type="date" name="end" value="<?php echo $end ?>">
                </div>
            </div>
        </div>
        <div class="SearchCalendar-group SearchCalendar-btn">
            <input type="submit">
        </div>
    </form>
<?php
    if ( $library ) :
        switch_to_blog( $library );
        $query = new WP_Query(
            $options
        );
        restore_current_blog();

        while ( $query->have_posts() ) :
            $query->next_post();
            $posts[] = array(
                'post' => $query->post,
                'url' => get_the_permalink($query->post)
            );
        endwhile;
    else:
        $options['posts_per_page'] = -1;

        foreach ( $sites as $blog ) :
            switch_to_blog( $blog->blog_id );
            $query = new WP_Query(
                $options
            );
            restore_current_blog();

            while ( $query->have_posts() ) :
                $query->next_post();
                $posts[] = array(
                    'post' => $query->post,
                    'url' => get_the_permalink($query->post),
                    'blog_id' => $blog->blog_id
                );
            endwhile;
        endforeach;

        $max_posts_per_page = 15;
        $start_slice        = $paged === 1 ? 0                      : ($max_posts_per_page * $paged) - $max_posts_per_page;
        $end_slice          = $paged === 1 ? $max_posts_per_page    : ($max_posts_per_page * $paged);
        $total_items        = count($posts);
        $total_pages        = ceil($total_items / $max_posts_per_page);
        $posts              = array_slice($posts, $start_slice, $end_slice);
        $pagination         = _pagination($total_pages, $paged, $url_path, $query_string);
    endif;
?>

    <div class="Results">
        <ul class="BlockList">
            <?php foreach ( $posts as $networkpost ): ?>
                <?php
                    $image = null;

                    if (has_post_thumbnail( $networkpost['post']->ID )):
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $networkpost['post']->ID ), 'single-post-thumbnail' );
                        $image = $image[0];
                    else:
                        $image = "http://bibliotecasmedellin.gov.co/content/uploads/2017/10/imagen-defecto.jpg";
                    endif;
                ?>
                <li class="BlockList-item">
                    <h3 class="BlockList-itemTitle">
                        <a href="<?php echo $networkpost['url'] ?>">
                            <?php echo $networkpost['post']->post_title ?>
                        </a>
                    </h3>
                    <div class="BlockList-content">
                        <div class="BlockList-left" style="background: url(<?php echo $image; ?>) no-repeat center center; background-size: cover;">
                            <img src="<?php echo $image; ?>" alt="Alt de la imagn">
                        </div>
                        <div class="BlockList-right">
                            <p class="BlockList-itemDescription"><?php echo $networkpost['post']->post_content ?></p>
                        </div>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Lugar: </span>
                        <span class="BlockList-itemText--Small">
                            <?php echo event_location($networkpost['post']->ID, $library === false ? $networkpost['blog_id'] : $library); ?>
                        </span>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Fecha: </span>
                        <span class="BlockList-itemText--XSmall">
                            <?php echo event_start_date($networkpost['post']->ID, $library === false ? $networkpost['blog_id'] : $library); ?>
                            <?php echo event_start_time($networkpost['post']->ID, $library === false ? $networkpost['blog_id'] : $library); ?>
                        </span>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Dirección: </span>
                        <span class="BlockList-itemText--XSmall">
                            <?php echo event_location_adress($networkpost['post']->ID, $library === false ? $networkpost['blog_id'] : $library); ?>
                        </span>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="pagination">
            <?php if ( $library ) : ?>
                <?php 
                    echo paginate_links( array(
                        'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, $paged ),
                        'total' => $query->max_num_pages
                    ) );
                ?>
            <?php else: ?>
                <?php echo $pagination; ?>
            <?php endif; ?>
        </div>
    </div>
</div> <!--/wrap-->
<?php get_footer(); ?>
