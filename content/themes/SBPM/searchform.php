<form action="<?php bloginfo('siteurl'); ?>" class="search-form SearchForm">
	<div class="row">
		<div class="vc_col-md-8 SearchForm-textContainer">
		    <div class="input-wrap search">
		        <input type="search" id="input-s" name="s" />
		    </div>
		</div>
		<div class="vc_col-md-4 SearchForm-submitContainer">
		    <div class="input-wrap submit">
		        <input type="submit" value="Buscar" class="button" />
			</div>
		</div>
	</div>
</form>