<?php
if ( ! function_exists( 'event_audiences' ) ) {
    // Register Custom Taxonomy
    function event_audiences() {

    	$labels = array(
    		'name'                       => _x( 'Públicos', 'Taxonomy General Name', 'dc_event_category' ),
    		'singular_name'              => _x( 'Público', 'Taxonomy Singular Name', 'dc_event_category' ),
    		'menu_name'                  => __( 'Públicos', 'dc_event_category' ),
    		'all_items'                  => __( 'Todas', 'dc_event_category' ),
    		'parent_item'                => __( 'Parent Item', 'dc_event_category' ),
    		'parent_item_colon'          => __( 'Parent Item:', 'dc_event_category' ),
    		'new_item_name'              => __( 'Nueva Categoría', 'dc_event_category' ),
    		'add_new_item'               => __( 'Añadir Categoría', 'dc_event_category' ),
    		'edit_item'                  => __( 'Editar', 'dc_event_category' ),
    		'update_item'                => __( 'Actualizar', 'dc_event_category' ),
    		'view_item'                  => __( 'Ver', 'dc_event_category' ),
    		'separate_items_with_commas' => __( 'Separate items with commas', 'dc_event_category' ),
    		'add_or_remove_items'        => __( 'Add or remove items', 'dc_event_category' ),
    		'choose_from_most_used'      => __( 'Choose from the most used', 'dc_event_category' ),
    		'popular_items'              => __( 'Popular Items', 'dc_event_category' ),
    		'search_items'               => __( 'Search Items', 'dc_event_category' ),
    		'not_found'                  => __( 'Not Found', 'dc_event_category' ),
    		'no_terms'                   => __( 'No items', 'dc_event_category' ),
    		'items_list'                 => __( 'Items list', 'dc_event_category' ),
    		'items_list_navigation'      => __( 'Items list navigation', 'dc_event_category' ),
    	);
    	$args = array(
    		'labels'                     => $labels,
    		'hierarchical'               => true,
    		'public'                     => true,
    		'show_ui'                    => true,
    		'show_admin_column'          => true,
    		'show_in_nav_menus'          => true,
    		'show_tagcloud'              => true,
    	);
    	register_taxonomy( 'event_audiences', array( 'event', 'event-recurring' ), $args );

    }
    add_action( 'init', 'event_audiences', 0 );
}

if( !function_exists('event_start_time')){
    function event_start_time($post_id, $blog_id){
        switch_to_blog( $blog_id );
        $time = get_post_meta($post_id, '_event_start_time', true);
        restore_current_blog();
        if(empty($time)){
            return 'Todo el día';
        }
        return $time;
    }
}

if( !function_exists('event_start_date')){
    function event_start_date($post_id, $blog_id){
        switch_to_blog( $blog_id );
        $date = get_post_meta($post_id, '_event_start_date', true);
        restore_current_blog();
        return $date;
    }
}

if( !function_exists('event_location')){
    function event_location( $post_id, $blog_id ) {
        global $wpdb;

        switch_to_blog( $blog_id );

        $location_id = get_post_meta( $post_id, '_location_id', true );

        restore_current_blog();
        switch_to_blog( 1 );

        $sql = "SELECT * FROM {$wpdb->prefix}em_locations WHERE location_id = {$location_id}";
        $l = $wpdb->get_row( $sql );

        if ( !empty( $l ) ) {
            restore_current_blog();
            return "{$l->location_name}, {$l->location_address} {$l->location_town}";
        }

        return;
    }
}

if( !function_exists('event_location_adress')){
    function event_location_adress ( $post_id, $blog_id ) {
        global $wpdb;

        switch_to_blog( $blog_id );

        $location_id = get_post_meta($post_id, '_location_id', true);

        restore_current_blog();
        switch_to_blog( 1 );

        $sql = "SELECT * FROM {$wpdb->prefix}em_locations WHERE location_id = {$location_id}";
        $l = $wpdb->get_row( $sql );

        if ( !empty( $l ) ) {
            restore_current_blog();
            return "{$l->location_address}";
        }

        return;
    }
}
?>
