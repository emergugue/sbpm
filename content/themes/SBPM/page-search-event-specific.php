<?php
/* Template Name: Buscador Específico Eventos */
$terms    = (!empty($_GET['terms']))    ? $_GET['terms']            : false;
$category = (!empty($_GET['category'])) ? $_GET['category']         : false;
$audience = (!empty($_GET['audience'])) ? $_GET['audience']         : false;
$start    = (!empty($_GET['start']))    ? $_GET['start']            : date('Y-m-d');
$end      = (!empty($_GET['end']))      ? $_GET['end']              : date('Y-m-d', strtotime('+1 years'));
$paged    = (get_query_var('paged'))    ? get_query_var('paged')    : 1;
$posts    = array();
$blog_id  = get_current_blog_id();

$categories = get_terms( array(
    'taxonomy' => 'event-categories',
    'hide_empty' => false,
));

$audiences = get_terms( array(
    'taxonomy' => 'event_audiences',
    'hide_empty' => false,
));

$options = array(
    'post_type' => 'event',
    'posts_per_page' => 15,
    'paged' => $paged,
    'order_by' => '_event_start_date'
);

$options['tax_query'] = array();

if( $terms ){
    $options['s'] = strip_tags(trim($terms));
}

if( $category ){
    $options['tax_query'][] = 		array(
		'taxonomy' => 'event-categories',
		'terms'    => (int) $category,
	);
}

if( $audience ){
    $options['tax_query'][] = 		array(
		'taxonomy' => 'event_audiences',
		'terms'    => $audience,
	);
}

if( $start ){
    if($end){
        $options['meta_key']     = '_event_start_date';
        $options['meta_value']   = array($start, $end);
        $options['meta_compare'] = 'BETWEEN';
    } else {
        $options['meta_key']     = '_event_start_date';
        $options['meta_value']   = $start;
        $options['meta_compare'] = '=';
    }
} else {
    $options['meta_key']     = '_event_start_date';
    $options['meta_value']   = date('Y-m-d');
    $options['meta_compare'] = '=';
}

get_header(); ?>

<div class="wrap row">
    <h2><?php the_title(); ?></h2>
    <?php get_template_part('loop', 'page'); ?>
    <form class="SearchCalendar">
        <div class="SearchCalendar-cols SearchCalendar-cols-3">
            <div class="SearchCalendar-group SearchCalendar-col-1">
                <label for="terms">Búsqueda por palabra clave</label>
                <input type="text" name="terms" placeholder="Ingrese una palabra clave" value="<?php echo $terms ?>">
            </div>
            <div class="SearchCalendar-group SearchCalendar-col-2">
                <label for="category">Selecciona el tipo de evento</label>
                <select name="category">
                    <option value>Categoría</option>
                    <?php foreach( $categories as $cat ): ?>
                        <option value="<?= $cat->term_id ?>" <?php echo ($cat->term_id == $category) ? 'selected' : '' ?>><?= $cat->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="SearchCalendar-group SearchCalendar-col-3">
                <label for="audience">Selecciona el tipo de público</label>
                <select name="audience">
                    <option value>Público</option>
                    <?php foreach( $audiences as $a ): ?>
                        <option value="<?= $a->term_id ?>" <?php echo ($a->term_id == $audience) ? 'selected' : '' ?>><?= $a->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="SearchCalendar-group">
            <p class="SearchCalendar-textLabel">Selecciona el rango de fechas</p>
            <div class="SearchCalendar-cols">
                <div class="SearchCalendar-col1">
                    <label for="start">De</label>
                    <input type="date" name="start" value="<?php echo $start ?>">
                </div>
                <div class="SearchCalendar-col2">
                    <label for="end">A</label>
                    <input type="date" name="end" value="<?php echo $end ?>">
                </div>
            </div>
        </div>
        <div class="SearchCalendar-group SearchCalendar-btn">
            <input type="submit">
        </div>
    </form>
    <h3 style="text-align: center;">¿No encuentras lo que buscas?</h3><a class="btn btn-volver" href="http://bibliotecasmedellin.gov.co/cms/agenda">Ir al buscador general de eventos</a>
<?php
    $query = new WP_Query(
        $options
    );

    while ( $query->have_posts() ) {
        $query->next_post();
        $posts[] = array(
            'post' => $query->post,
            'url' => get_the_permalink($query->post)
        );
    }
?>

    <div class="Results">
        <ul class="BlockList">
            <?php foreach ( $posts as $networkpost ): ?>
                <?php
                    $image = null;

                    if (has_post_thumbnail( $networkpost['post']->ID )):
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $networkpost['post']->ID ), 'single-post-thumbnail' );
                        $image = $image[0];
                    else:
                        $image = "http://bibliotecasmedellin.gov.co/content/uploads/2017/10/imagen-defecto.jpg";
                    endif;
                ?>
                <li class="BlockList-item">
                    <h3 class="BlockList-itemTitle">
                        <a href="<?php echo $networkpost['url'] ?>"><?php echo $networkpost['post']->post_title ?></a>
                    </h3>
                    <div class="BlockList-content">
                        <div class="BlockList-left" style="background: url(<?php echo $image; ?>) no-repeat center center; background-size: cover;">
                            <img src="<?php echo $image; ?>" alt="Alt de la imagn">
                        </div>
                        <div class="BlockList-right">
                            <p class="BlockList-itemDescription"><?php echo $networkpost['post']->post_content ?></p>
                        </div>
                    </div>   
                    <div class="BlockList-itemText">
                        <span class="TextBold">Lugar: </span>
                        <span class="BlockList-itemText--Small">
                            <?php echo event_location($networkpost['post']->ID, $blog_id); ?>
                        </span>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Fecha: </span>
                        <span class="BlockList-itemText--XSmall">
                            <?php echo event_start_date($networkpost['post']->ID, $blog_id); ?>
                        </span>
                    </div>
                    <div class="BlockList-itemText">
                        <span class="TextBold">Dirección: </span>
                        <span class="BlockList-itemText--XSmall">
                            <?php echo event_location_adress($networkpost['post']->ID, $blog_id); ?>
                        </span>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="pagination">
            <?php 
                echo paginate_links( array(
                    'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'format' => '?paged=%#%',
                    'current' => max( 1, $paged ),
                    'total' => $query->max_num_pages
                ) );
            ?>
        </div>
    </div>
</div> <!--/wrap-->
<?php get_footer(); ?>
