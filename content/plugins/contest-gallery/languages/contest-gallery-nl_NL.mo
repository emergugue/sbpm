��    ?        Y         p     q     y     �     �     �     �     �     �  	   �     �      �               $  !   =  !   _  !   �  %   �  %   �  %   �            6      W  *   x     �  *   �     �  &   �               .     @     W     h  8   x     �     �     �     �     �     �     �     �  7   	  2   P	     �	     �	  0   �	  (   �	     
     /
     M
  
   l
     w
  !   �
     �
  
   �
     �
  $   �
  '         :     [  �  h     +     3  
   F  
   Q     \     n     �     �     �     �  A   �               %  +   <  +   h  +   �  !   �  !   �  !     *   &  *   Q  *   |  )   �     �  )   �       2     
   J     U     g     x     �     �  4   �  	   �     �                8  	   O  	   Y     c  ,   {  )   �     �     �  G     &   S     z  #   �  $   �  
   �     �  '         +     L     T  !   l     �  &   �     �                           6   2   1          )             7                            $   *                     "   0   	   .          3             =      '         5   4       %      (       ?   &              <         !   ;          >         -       8   ,                  +   9             :   #       
       /          Account Back to gallery Comment Comments Comments ascend Comments descend Date ascend Date descend Full size Height view Hold left mouse to see user info I am not a robot Logout? Max amount of characters Maximum allowed height for GIF is Maximum allowed height for JPG is Maximum allowed height for PNG is Maximum allowed resolution for GIF is Maximum allowed resolution for JPG is Maximum allowed resolution for PNG is Maximum allowed width for GIF is Maximum allowed width for JPG is Maximum allowed width for PNG is Maximum number of images for one upload is Min amount of characters Minimum number of images for one upload is Name Only registered users are able to vote Password Password is wrong Password required Passwords do not match Picture comments Please fill out Plz check the checkbox to prove that you are not a robot Random Rating ascend Rating descend Row view Select your image Send Sort by Thank you for your comment The comment field must contain three characters or more The name field must contain two characters or more The photo contest is over The resolution of this image is The selected file is too large, max allowed size This email address is already registered This email is not valid This file type is not allowed This username is already taken Thumb view Username or e-mail Username or e-mail does not exist Username or e-mail required Vote first You are already logged in You have already used all your votes You have already voted for this picture You have to check this agreement Your comment Project-Id-Version: Contest Gallery
POT-Creation-Date: 2017-09-11 14:51+0200
PO-Revision-Date: 2017-09-11 14:51+0200
Last-Translator: 
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: index.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Account Terug naar Galerie Commentaar Commentaar Reacties oplopend Reacties aflopend Datum oplopend Datum aflopen Ware Grootte Alle foto's op dezelfde hoogte Linker muisknop ingedrukt om informatie voor de gebruiker te zien Ik ben geen robot Logout? Maximum aantal letters De maximale resolutie de hoogte voor GIF is De maximale resolutie de hoogte voor JPG is De maximale resolutie de hoogte voor PNG is De maximale resolutie voor GIF is De maximale resolutie voor JPG is De maximale resolutie voor PNG is De maximale resolutie de brede voor GIF is De maximale resolutie de brede voor JPG is De maximale resolutie de brede voor PNG is Maximum aantal beelden voor een upload is Minimum aantal letters Minimum aantal beelden voor een upload is Naam Alleen geregistreerde gebruikers kunnen een cijfer Wachtwoord Password  is vals Wachtwoord nodig Wachtwoorden komen niet overeen Commentaar op de foto Vul Zet een vinkje om aan te geven dat je geen robot ben Zoevallig Recensies oplopend Recensies aflopend Gelijk aantal beelden in een rij Geen foto geselecteerd Versturen Sortieren Bedankt voor uw reactie De reactie moet langer zijn dan drie letters De naam moet langer zijn dan twee letters De fotowedstrijd is voltooid De resolutie van de foto is Het bestand dat u hebt gekozen is te groot. Maximaal toegestane grootte Dit e-mailadres is reeds geregistreerd Het e-mailadres is ongeldig Dit bestandstype is niet toegestaan Deze gebruikersnaam is al in gebruik Miniaturen Gebruikersnaam of e-mail Gebruikersnaam oder e-Mail bestaat niet Gebruikersnaam oder e-mail nodig Stemmen U bent al geregistreerd Je hebt al gebruikt al uw stemmen Deze foto heb je al beoordeeld U moet akkoord gaan met de voorwaarden Uw Commentaar 