��    =        S   �      8     9     I     Q     Z     j     {     �  	   �     �      �     �     �  !   �  !     !   9  %   [  %   �  %   �      �      �        *   0     [  *   t     �  &   �     �     �     �     �             8   0     i     p     ~     �     �     �     �     �  7   �  2   	     ;	     U	  0   u	  (   �	     �	     �	     
  
   $
     /
  !   B
     d
  
   �
     �
  $   �
  '   �
      �
       �        �  
   �               "     4     E     W     h  T   {     �     �  '   �  '   '  '   O  ,   w  ,   �  ,   �  &   �  &   %  &   L  2   s     �  2   �     �  *   �     *     6     S     k     �     �  K   �  	                  6     G     \     c     o  ;   �  6   �  %   �      "  I   C  :   �  &   �  '   �  &        >  '   M  4   u  1   �     �     �           "  &   >     e         !   6             	       -                 9   &          8         1   /   "   =         ,   $              7   #      ;                          )      :   4      +             <       0                    *          .                     3      2          5   %      
   (      '              Back to gallery Comment Comments Comments ascend Comments descend Date ascend Date descend Full size Height view Hold left mouse to see user info I am not a robot Max amount of characters Maximum allowed height for GIF is Maximum allowed height for JPG is Maximum allowed height for PNG is Maximum allowed resolution for GIF is Maximum allowed resolution for JPG is Maximum allowed resolution for PNG is Maximum allowed width for GIF is Maximum allowed width for JPG is Maximum allowed width for PNG is Maximum number of images for one upload is Min amount of characters Minimum number of images for one upload is Name Only registered users are able to vote Password Password is wrong Password required Passwords do not match Picture comments Please fill out Plz check the checkbox to prove that you are not a robot Random Rating ascend Rating descend Row view Select your image Send Sort by Thank you for your comment The comment field must contain three characters or more The name field must contain two characters or more The photo contest is over The resolution of this image is The selected file is too large, max allowed size This email address is already registered This email is not valid This file type is not allowed This username is already taken Thumb view Username or e-mail Username or e-mail does not exist Username or e-mail required Vote first You are already logged in You have already used all your votes You have already voted for this picture You have to check this agreement Your comment Project-Id-Version: Contest Gallery
POT-Creation-Date: 2017-09-11 14:51+0200
PO-Revision-Date: 2017-09-11 14:51+0200
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: index.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Volver a la galería Comentario Comentarios Comentarios asc Comentarios bajar Fecha de ascenso Fecha de descenso Tamaño completo Vista de la altura Mantenga pulsado el botón izquierdo del ratón para ver la información del usuario No soy un robot Cantidad máxima de caracteres La altura máxima permitida para GIF es La altura máxima permitida para JPG es La altura máxima permitida para PNG es La resolución máxima permitida para GIF es La resolución máxima permitida para JPG es La resolución máxima permitida para PNG es El ancho máximo permitido para GIF es El ancho máximo permitido para JPG es El ancho máximo permitido para PNG es El número máximo de imágenes para una subida es Cantidad mínima de caracteres El número mínimo de imágenes para una subida es Nombre Solo los usuarios registrados pueden votar Contraseña La contraseña es incorrecta Se requiere contraseña Las contraseñas no coinciden Comentarios de la imagen Por favor complete Plz marque la casilla de verificación para probar que usted no es un robot Aleatorio Puntuación ascend Puntuación descendente Vista de la fila Seleccione su imagen Enviar Ordenar por Gracias por tu comentario El campo de comentario debe contener tres caracteres o más El campo de nombre debe contener dos caracteres o más El concurso fotográfico ha terminado La resolución de esta imagen es El archivo seleccionado es demasiado grande, el tamaño máximo permitido Esta dirección de correo electrónico ya está registrada Este correo electrónico no es válido Este tipo de archivo no está permitido Este nombre de usuario ya está en uso Thumb ver más Nombre de usuario o correo electrónico El nombre de usuario o correo electrónico no existe Nombre de usuario o correo electrónico requerido Votar primero Ya se ha autentificado Ya has utilizado todos tus votos Ya has votado por esta foto Usted tiene que comprobar este acuerdo Tu comentario 