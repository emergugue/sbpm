��    ?        Y         p     q     y     �     �     �     �     �     �  	   �     �      �               $  !   =  !   _  !   �  %   �  %   �  %   �            6      W  *   x     �  *   �     �  &   �               .     @     W     h  8   x     �     �     �     �     �     �     �     �  7   	  2   P	     �	     �	  0   �	  (   �	     
     /
     M
  
   l
     w
  !   �
     �
  
   �
     �
  $   �
  '         :     [  �  h     +     3  	   G  
   Q     \     s     �     �     �     �  ,   �       
          (   9  (   b  (   �  %   �  %   �  %      )   &  )   P  )   z  .   �     �  .   �       &   #     J     S     g  "        �     �  <   �  	             &  (   =     f     y  	   �     �  3   �  .   �          &  G   D  ,   �  "   �  !   �  (   �     '     5  ,   N  (   {     �     �  $   �  +   �  /        I                           6   2   1          )             7                            $   *                     "   0   	   .          3             =      '         5   4       %      (       ?   &              <         !   ;          >         -       8   ,                  +   9             :   #       
       /          Account Back to gallery Comment Comments Comments ascend Comments descend Date ascend Date descend Full size Height view Hold left mouse to see user info I am not a robot Logout? Max amount of characters Maximum allowed height for GIF is Maximum allowed height for JPG is Maximum allowed height for PNG is Maximum allowed resolution for GIF is Maximum allowed resolution for JPG is Maximum allowed resolution for PNG is Maximum allowed width for GIF is Maximum allowed width for JPG is Maximum allowed width for PNG is Maximum number of images for one upload is Min amount of characters Minimum number of images for one upload is Name Only registered users are able to vote Password Password is wrong Password required Passwords do not match Picture comments Please fill out Plz check the checkbox to prove that you are not a robot Random Rating ascend Rating descend Row view Select your image Send Sort by Thank you for your comment The comment field must contain three characters or more The name field must contain two characters or more The photo contest is over The resolution of this image is The selected file is too large, max allowed size This email address is already registered This email is not valid This file type is not allowed This username is already taken Thumb view Username or e-mail Username or e-mail does not exist Username or e-mail required Vote first You are already logged in You have already used all your votes You have already voted for this picture You have to check this agreement Your comment Project-Id-Version: Contest Gallery
POT-Creation-Date: 2017-09-11 14:50+0200
PO-Revision-Date: 2017-09-11 14:50+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: index.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Account Zurück zur Galerie Kommentar Kommentare Kommentare aufsteigend Kommentare absteigend Datum aufsteigend Datum absteigend Volle Größe Alle Bilder gleiche Höhe Linke Maustaste halten um User-Info zu sehen Ich bin kein Bot Ausloggen? Höchstanzahl an Buchstaben Die maximal erlaubte Höhe für GIFs ist Die maximal erlaubte Höhe für JPGs ist Die maximal erlaubte Höhe für PNGs ist Die maximale Auflösung für GIFs ist Die maximale Auflösung für JPGs ist Die maximale Auflösung für PNGs ist Die maximal erlaubte Breite für GIFs ist Die maximal erlaubte Breite für JPGs ist Die maximal erlaubte Breite für PNGs ist Maximale Anzahl der Bilder für ein Upload ist Mindestanzahl an Buchstaben Minimale Anzahl der Bilder für ein Upload ist Name Nur registrierte User dürfen bewerten Passwort Passwort ist falsch Bitte Passwort eingeben Passwörter stimmen nicht überein Kommentare zum Bild Bitte ausfüllen Bitte Häckchen setzten um zu zeigen, dass Sie kein Bot sind Zufällig Bewertungen aufsteigend Bewertungen absteigend Gleiche Anzahl an Bildern in einer Reihe Kein Bild gewählt Senden Sortieren Danke für dein Kommentar Der Kommentar muss länger als drei Buchstaben sein Der Name muss länger als zwei Buchstaben sein Der Fotowettbewerb ist beendet Die Auflösung des Bildes ist Die Datei die Sie gewählt haben ist zu groß. Maximal erlaubte Größe Diese E-Mail Adresse ist bereits registriert Diese E-Mail Adresse ist ungültig Dieser File Typ ist nicht erlaubt Dieser Benutzername ist bereits vergeben Thumb Ansicht Benutzername oder E-Mail Benutzername oder E-Mail ist nicht vorhanden Bitte Benutzernamen oder E-Mail eingeben Bewerte Sie sind bereits eingeloggt Du hast all deine Stimmen verbraucht Du hast für dieses Bild bereits abgestimmt Du musst mit den Bedingungen einverstanden sein Dein Kommentar 